import sysconfig
from ConfigParser import SafeConfigParser
from path import Path
from datetime import date, timedelta
import subprocess, os


def config(conf_path=None):
    """
    Read the configuration file
    :param conf_path: Path to the file. Default: './monitor-dgw.cfg'
    :return: dictionary with DB parameters, Path to the directories, url of the servers
    """
    if not conf_path:
        config_file=Path('./fnr.cfg')
    else:
        config_file = Path(conf_path)
    if not config_file.exists():
        raise IOError("Config file not found")
    else:
        config = SafeConfigParser()
        config.read(config_file)


        server = {'GLASS': config.get("server", "GLASS"),
                  }

        directory = {'STOREDIR': config.get("directory", "STOREDIR"),
                  }

        paths = {'PYGLASS': config.get("path", "PYGLASS"),
                     }
        config_dict = {'server': server, 'paths': paths, 'directory':directory}
        return config_dict


def get_file_list(server, day):
    # start = today.isoformat()
    # end = today - timedelta(days = day)
    
    file_list = subprocess.check_output([Path.joinpath(PATH,'pyglass.py'), 'files', '-u', server, '-dds', day,  '-dde',day, '-o', 'short-csv' ])

    return file_list

def diff_files(file_list, day, storedir):
    nouv_liste = []
    orig_liste = []
    with open(os.path.join(storedir, day+'.previous')) as f:
        for l in f.read().splitlines()[1:-1]:
            temp = l.split(',')
            orig_liste.append([temp[0].strip(), temp[-2].strip()])
    
    for l in file_list.splitlines()[1:-1]:
        temp = l.split(',')
        nouv_liste.append([temp[0].strip(), temp[-2].strip()])
    diff_list = []
    for i in nouv_liste:
        flag = False
        for j in orig_liste:
            if i[0] == j[0]:
                flag = True
                if i[1] == j[1]:
                    continue
                else:
                    diff_list.append(j[0]) 
        if flag is False:
            diff_list.append(i [0])
    return diff_list  


def check_new(server, day, storedir):
    file_list = get_file_list(server, day)
    if not os.path.exists(os.path.join(storedir,day+'.previous')):
        with open(os.path.join(storedir, day+'.previous'), 'w') as f:
            f.write(file_list)
    else:
        diff_list = diff_files(file_list, day, storedir)
        with open(os.path.join(storedir, day+'.previous'), 'w') as f:
            f.write(file_list)
        return diff_list


if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser(description='Run the find new rinex tool')
    # parser.add_argument('--stations', nargs='+', action='store', help='')  #
    # parser.add_argument('--stations-file', action='store', help='')  #
    parser.add_argument('day', action='store', help='')
    parser.add_argument('--conf', action='store', help='configuration file path')

    args = parser.parse_args()
    config_dict = config(args.conf)
    PATH = config_dict['paths']['PYGLASS']
    diff_list = check_new(server=config_dict['server']['GLASS'],  day=args.day, storedir = config_dict['directory']['STOREDIR'])
    if diff_list:
        for i in diff_list:
            print i
